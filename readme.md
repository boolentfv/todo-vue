
![IMAGE_DESCRIPTION](screen.png)
## Настройка
Для запуска dev сервер необходимо определить переменную окружения `VITE_BASE_URL`.

```
export VITE_BASE_URL = "https://jsonplaceholder.typicode.com"

```
## Основные скрипты

Дев-сервер можно запустить с помощью команды `yarn run dev`.

Билд проекта осуществляется командой  `yarn run build`.
