import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import process from "process";
import path from "path";

export default defineConfig({
    base: process.env.VITE_BASE_URL,
    resolve: {
        alias:{
            '@' : `${path.resolve(__dirname, './src')}`,
            '@use' : `${path.resolve(__dirname, './src/use')}`,
            '@components' : `${path.resolve(__dirname, './src/components')}`,
            '@store' : `${path.resolve(__dirname, './src/store')}`,
            '@assets' : `${path.resolve(__dirname, './src/assets')}`,
            '@utils' : `${path.resolve(__dirname, './src/utils')}`
        },
    },
    plugins: [vue()]
});