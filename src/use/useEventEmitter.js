
import { onMounted, onUnmounted } from 'vue';
import EventEmitter from 'event-emitter';

const emitter = new EventEmitter();


export function useEventEmitter() {
    return emitter;
}
export function useSubscription(event, listener) {
    onMounted(() => {
        emitter.on(event, listener);
    });
    onUnmounted(() => {
        emitter.of(event, listener);
    });
}