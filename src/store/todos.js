import { TODOS_URL } from '@/api';
import { getIdGenerator } from '@utils/generators';

const getId = getIdGenerator(300);
// Create a new store instance.
export const todos = {
    state () {
        return {
            filters: {
                showPanel: false,
                search: "",
                showIncompleteOnly: true
            },
            todos: []
        };
    },
    actions: {
        async fetchTodos ({commit}) {
            const response = await fetch(TODOS_URL);
            const data = await response.json();
            if(response.ok) {
                commit('setTodos', data);
            } else {
                throw new Error(JSON.stringify(data));
            }
        },
        async createTodo ({commit}, title) {
            const todo = {
                title,
                completed: false
            };
            const response = await fetch(TODOS_URL, {
                method: "POST",
                data: JSON.stringify(todo)
            });
            const data = await response.json();
            if(response.ok) {

                /*
                    Мок сервер всегда возвращает одинаковый id,
                    поэтому будем генерировать его сами
                */
                commit('addTodo', {...todo, id: getId()});
            } else {
                throw new Error(JSON.stringify(data));
            }
        },
        async toggleTodo ({commit}, todo) {
            const response = await fetch(`${TODOS_URL}${todo.id}/`, {
                method: "PATCH",
                data: JSON.stringify({completed: !todo.completed})
            });
            const data = await response.json();
            if(response.ok) {
                commit('toggleTodo', todo.id);
            } else {
                throw new Error(JSON.stringify(data));
            }
        },
    },
    mutations: {
        setTodos (state, todos) {
            state.todos = todos;
        },
        addTodo (state, todo) {
            state.todos.unshift(todo);
        },
        toggleTodo (state, todoId) {
            const todo = state.todos.find(todo => todo.id === todoId);
            todo.completed = !todo.completed;
        },
        setSearch (state, search) {
            state.filters.search = search;
        },
        setShowIncompleteOnly (state, showIncompleteOnly) {
            state.filters.showIncompleteOnly = showIncompleteOnly;
        },
        toggleFilterPanel (state) {
            state.filters.showPanel = !state.filters.showPanel;
        }
    },
    getters: {
        search: (state) => state.filters.search,
        showIncompleteOnly: (state) => state.filters.showIncompleteOnly,
        filteredTodos: (state, {search, showIncompleteOnly}) => {
            const todos = state.todos;
            return todos
                .filter(({title, completed}) => {
                    const show = !showIncompleteOnly || !completed;
                    return title.toLowerCase().includes(search.toLowerCase()) && show;
                });
        },
        showFilterPanel: (state) =>  state.filters.showPanel
    }
};