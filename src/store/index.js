import { createStore } from 'vuex';
import { todos } from '@store/todos';

// Create a new store instance.
export const store = createStore({
    modules: {
        todos
    }
});