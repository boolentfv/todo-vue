export function getIdGenerator(firstId = 0) {
    return () => {
        return firstId++;
    };
}
