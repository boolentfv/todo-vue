export const BASE_URL = import.meta.env.VITE_BASE_URL;
export const TODOS_URL = `${BASE_URL}/todos/`;