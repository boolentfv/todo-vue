import {createApp} from 'vue';

import App from '@/App.vue';
import { store } from '@store';
import 'vue3-toastify/dist/index.css';
import '@assets/css/variables.css';
import '@assets/css/style.css';

const app = createApp(App);
app.use(store);
app.mount('#app');
